public class NodeHT<K, V> {
    private K _key;
    private V _value;

    NodeHT(K key, V value) {
        setKey(key);
        setValue(value);
    }

    public K getKey() { return _key; }

    public void setKey(K _key) { this._key = _key; }

    public V getValue() { return _value; }

    public void setValue(V _value) { this._value = _value; }
}
