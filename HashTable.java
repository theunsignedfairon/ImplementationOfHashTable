import java.util.*;

public class HashTable<K, V> {
    private final double DEFAULT_LOAD_FACTOR = 0.75;
    private final int MAX_SHIFT = 10;

    private ArrayList<NodeHT<K, V>> table;
    private ArrayList<K> keySet;

    private double _loadFactor;
    private int _capacity;
    private int _size;

    HashTable(int initCapacity) {
        setCapacity(getPrimeNumber(initCapacity));
        setSize(0);
        table = new ArrayList<>(initCapacity);
        keySet = new ArrayList<>();
        initNullArrayList(table, getCapacity());
    }

    private long hashCode(Object key) {
        long hashCode;
        return (key == null) ? 0 : (hashCode = key.hashCode()) ^ (hashCode >>> 16);
    }

    private boolean checkLoadFactor() { return calculateLoadFactor() <= DEFAULT_LOAD_FACTOR; }

    private double calculateLoadFactor() {
        setLoadFactor((double)getSize()/getCapacity());
        return getLoadFactor();
    }

    private int compressionFunc(long hashCode) { return (int)hashCode % getCapacity(); }

    public boolean isEmpty() {
        return getSize() == 0;
    }

    public boolean containsKey(Object key) {
        for (int index = 0; index < keySet.size(); index++) {
            if (keySet.get(index).equals(key))
                return true;
        }
        return false;
    }

    public boolean containsValue(Object value) {
        for (int index = 0; index < keySet.size(); index++) {
            if (get(keySet.get(index)).equals(value)) {
                return true;
            }
        }
        return false;
    }

    public V get(K key) {
        if (!containsKey(key)) throw new NoSuchElementException();
        int shiftIndex = 0;
        while (!table.get(compressionFunc(hashCode(key) + (int)Math.pow(shiftIndex, 2))).getKey().equals(key)) {
            shiftIndex++;
        }
        return table.get(compressionFunc(hashCode(key) + (int)Math.pow(shiftIndex, 2))).getValue();
    }

    public void put(K key, V value) {
        if (containsKey(key)) {
            table.get(compressionFunc(hashCode(key))).setValue(value);
        } else {
            NodeHT<K, V> node = new NodeHT<>(key, value);
            if (table.get(compressionFunc(hashCode(key))) == null) {
                table.set(compressionFunc(hashCode(key)), node);
                keySet.add(key);
                setSize(getSize() + 1);
            } else {
                int shiftIndex = 1;
                while (table.get(compressionFunc(hashCode(key) + (int) Math.pow(shiftIndex, 2))) != null) {
                    shiftIndex++;
                    if (shiftIndex > MAX_SHIFT) {
                        resize();
                        shiftIndex = 0;
                    }
                }
                table.set(compressionFunc(hashCode(key) + (int) Math.pow(shiftIndex, 2)), node);
                keySet.add(key);
                setSize(getSize() + 1);
            }
            if (!checkLoadFactor()) resize();
        }
    }

    public V remove(Object key) {
        return null;
    }

    public void clear() {
        initNullArrayList(table, getCapacity());
        initNullArrayList(keySet, keySet.size());
    }

    public Collection<K> keySet() { return keySet; }

    public Collection<V> values() {
        ArrayList<V> values = new ArrayList<V>();
        for (int index = 0; index < keySet.size(); index++) { values.add(get(keySet.get(index))); }
        return values;
    }

    public Collection<Pair<K,V>> entrySet() {
        ArrayList<Pair<K, V>> entrySet = new ArrayList<>();
        for (int index = 0; index < keySet.size(); index++) {
            entrySet.add(new Pair<K, V>(keySet.get(index), get(keySet.get(index))));
        }
        return entrySet;
    }

    private double getLoadFactor() { return _loadFactor; }
    public int getSize() { return _size; }
    private int getCapacity() { return _capacity; }
    private ArrayList<NodeHT<K, V>> getTable() { return table; }

    private void setSize(int size) { _size = size; }
    private void setCapacity(int capacity) { _capacity = getPrimeNumber(capacity); }
    private void setLoadFactor(double loadFactor) { _loadFactor = loadFactor; }

    private void initNullArrayList(ArrayList<?> arrayList, int size) {
        arrayList.clear();
        for (int index = 0; index < size; index++) {
            arrayList.add(null);
        }
    }

    private void resize() {
        HashTable<K, V> tempHashTable = new HashTable<>(getCapacity() * 2);
        for (int index = 0; index < keySet.size(); index++) {
            tempHashTable.put(keySet.get(index), get(keySet.get(index)));
        }
        setCapacity(getCapacity() * 2);
        table = new ArrayList<>(tempHashTable.getTable());
    }

    private int getPrimeNumber(int number) {
        while (true) {
            if (checkOnPrimeNumber(number)) {
                return number;
            } else {
                number++;
            }
        }
    }

    private boolean checkOnPrimeNumber(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }
}
