import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        PrintWriter wr = new PrintWriter("output.txt");
        Scanner sc = new Scanner(new FileReader("exception_words.txt"));

        HashTable<String, Integer> exceptionWordsHT = new HashTable<>(10000);
        HashTable<String, Integer> counterWordsHT = new HashTable<>(10000);
        List<String> inputText = Files.readAllLines(Paths.get("input.txt"), StandardCharsets.UTF_8);
        ArrayList<Pair<String, Integer>> resultList = new ArrayList<>();

        while (sc.hasNext()) { exceptionWordsHT.put(sc.next(), 1); }

        for (int index = 0; index < inputText.size(); index++) { inputText.set(index, inputText.get(index).toLowerCase().replace('.',' ').replace(',', ' ')); }

        for (String line: inputText) {
            sc = new Scanner(line);
            while (sc.hasNext()) {
                String word = sc.next();
                if (!exceptionWordsHT.containsKey(word)) {
                    if (word.charAt(word.length() - 1) == 's') {
                        String subWord = word.substring(0, word.length() - 1);
                        if (containWord((ArrayList<String>) inputText, subWord) && !exceptionWordsHT.containsKey(subWord)) {
                            if (counterWordsHT.containsKey(subWord)) {
                                counterWordsHT.put(subWord, counterWordsHT.get(subWord) + 1);
                            } else {
                                counterWordsHT.put(subWord, 1);
                            }
                            continue;
                        }
                    }
                    if (counterWordsHT.containsKey(word)) {
                        counterWordsHT.put(word, counterWordsHT.get(word) + 1);
                    } else {
                        counterWordsHT.put(word, 1);
                    }
                }
            }
        }

        for (Pair<String, Integer> pair : counterWordsHT.entrySet()) { if (pair.getValue() > 1) resultList.add(pair); }

        sortPairsAlphabetics(resultList);

        for (int index = 0; index < resultList.size(); index++) {
            wr.print(  "<\"" + resultList.get(index).getKey() + "\", " + resultList.get(index).getValue()  + ">");
            if (index != resultList.size() - 1) wr.print("\n");
        }

        sc.close();
        wr.close();
    }

    public static void sortPairsAlphabetics(ArrayList<Pair<String, Integer>> arrayList) {
        for (int i = arrayList.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arrayList.get(j).getKey().compareTo(arrayList.get(j + 1).getKey()) > 0)
                    swap(arrayList, j, j + 1);
            }
        }
    }

    public static void swap(ArrayList<Pair<String, Integer>> arrayList, int i, int j) {
        Pair<String, Integer> buf = arrayList.get(i);
        arrayList.set(i, arrayList.get(j));
        arrayList.set(j, buf);
    }

    public static boolean containWord(ArrayList<String> arrayList, String word) {
        Scanner sc;
        for (String line: arrayList) {
            sc = new Scanner(line);
            while (sc.hasNext()) {
                if (sc.next().equals(word))
                    return true;
            }
        }
        return false;
    }
}
